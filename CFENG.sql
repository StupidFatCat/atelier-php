-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Client: mysql-tp.svc.enst-bretagne.fr
-- Généré le: Sam 28 Février 2015 à 17:39
-- Version du serveur: 5.0.45
-- Version de PHP: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `CFENG`
--

-- --------------------------------------------------------

--
-- Structure de la table `Personnes`
--

CREATE TABLE IF NOT EXISTS `Personnes` (
  `id` int(11) NOT NULL auto_increment,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(15) NOT NULL,
  `naissance` date NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `inscription` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `url` varchar(255) NOT NULL,
  `image` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Contenu de la table `Personnes`
--

INSERT INTO `Personnes` (`id`, `nom`, `prenom`, `naissance`, `sexe`, `inscription`, `url`, `image`) VALUES
(51, 'Feng', 'Cong', '1980-02-01', 'masculin', '2015-02-18 13:15:30', 'http://www.bbbb.com', 'photos/51.jpeg'),
(52, 'Chhim', 'Tiffany', '1998-03-22', 'féminin', '2015-02-17 08:18:34', 'Http://www.aaa.com', 'photos/52.jpeg'),
(53, 'CCCC', 'DDDD', '2001-04-01', 'féminin', '2015-02-28 15:45:04', 'http://dsfdf.com', 'photos/53.jpeg'),
(54, 'dfdfdfdfd', 'fdfdsf', '2015-02-05', 'masculin', '2015-02-28 15:47:11', 'http://dsfdf.com', 'photos/54.jpeg'),
(55, 'B', 'B', '1999-06-11', 'féminin', '2015-02-28 15:47:51', 'http://www.blabla.com', 'photos/55.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `Personnes_statut`
--

CREATE TABLE IF NOT EXISTS `Personnes_statut` (
  `id_pers` int(11) NOT NULL,
  `id_stat` int(11) NOT NULL,
  PRIMARY KEY  (`id_pers`,`id_stat`),
  KEY `id_pers` (`id_pers`,`id_stat`),
  KEY `id_stat` (`id_stat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Personnes_statut`
--

INSERT INTO `Personnes_statut` (`id_pers`, `id_stat`) VALUES
(52, 1),
(53, 1),
(54, 1),
(51, 2),
(53, 2),
(51, 3),
(52, 3),
(55, 4);

-- --------------------------------------------------------

--
-- Structure de la table `Statut`
--

CREATE TABLE IF NOT EXISTS `Statut` (
  `id` int(11) NOT NULL auto_increment,
  `occupation` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Contenu de la table `Statut`
--

INSERT INTO `Statut` (`id`, `occupation`) VALUES
(1, 'enseignant'),
(2, 'étudiant'),
(3, 'ingénieur'),
(4, 'retraité'),
(5, 'chômeur'),
(6, 'autre');

-- --------------------------------------------------------

--
-- Structure de la table `Visites`
--

CREATE TABLE IF NOT EXISTS `Visites` (
  `id` int(11) NOT NULL default '1',
  `compteur_visites` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Visites`
--

INSERT INTO `Visites` (`id`, `compteur_visites`) VALUES
(1, 27);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Personnes_statut`
--
ALTER TABLE `Personnes_statut`
  ADD CONSTRAINT `Personnes_statut_ibfk_1` FOREIGN KEY (`id_pers`) REFERENCES `Personnes` (`id`),
  ADD CONSTRAINT `Personnes_statut_ibfk_2` FOREIGN KEY (`id_stat`) REFERENCES `Statut` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

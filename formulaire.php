<?php 
$update = false;
if ($_GET['operation'] == 'update') {
    $update = true;
}
 ?>
<html>
<meta charset=utf-8>

<head>
<link rel="stylesheet" href="style.css">
<title>Formulaire</title>
</head>

<body>

<?php echo "Entrez vos informations : " ?>

<form action="resultat-formulaire.php" method="post">
    <table class="formulaire">
    <tbody>
        <tr>
            <th>Opération</th>
            <td class="impair"><input type="hidden" name="operation" <?php if ($update) {
                echo "value='update'";
            }else{
                echo "value='insert'";
            }
             ?>> <?php if ($update) {
                echo "Modifier";
            }else{
                echo "Ajouter";
            }
             ?></td>
        </tr>
        <tr>
            <?php  // if operation is update, we need to show the values. 
                include 'connect.php';
                if ($update) {
                    $update = true;
                    $id = $_GET['id'];
                    echo "<th>id</th><td class='pair'>";
                    echo "<input class='fill' type='number' name='id' id='' readonly value='$id' ></td>";
                    $sql = "SELECT p . * , s.occupation
                        FROM Personnes p
                        INNER JOIN Statut s
                        INNER JOIN Personnes_statut ps
                        WHERE p.id = ps.id_pers
                        AND s.id = ps.id_stat And p.id=$id";
                    $res = mysql_query($sql)
                                    or die("cette requête est invalide.");
                    $i = 0;
                while ($ligne = mysql_fetch_array($res)) {                  
                    $nom = $ligne['nom'];
                    $prenom = $ligne['prenom'];
                    $naissance = $ligne['naissance'];
                    $sexe = $ligne['sexe'];
                    $occupation[$i++] = $ligne['occupation'];
                    $url = $ligne['url'];
                    $inscription = $ligne['inscription'];
                }   
            } ?>
        </tr>
        <tr>
            <th>Nom</th> <td class="impair"><input class="fill" type="text" name="nom" size="20" <?php if ($update) {
                echo "value='$nom'";
            } ?>></td>
        </tr>
        <tr>
            <th>Prénom</th>  <td class="pair"><input class="fill" type="text" name="prenom" size="15" <?php if ($update) {
                echo "value='$prenom'";
            } ?>></td>
        </tr>
        <tr>
            <th>Date de naissance</th> <td class="impair"><input class="fill" type="date" placeholder="yyyy/mm/dd" name="naissance" <?php if ($update) {
                echo "value='$naissance'";
            } ?>></td>
        </tr>
        <tr>
            <th>Sexe</th>
            <td class="pair">
            <input type="radio" name="sexe" value="masculin" <?php if ($update and $sexe == 'masculin') {
                echo "checked";
            } ?>>Masculin
            <input type="radio" name="sexe" value="féminin" <?php if ($update and $sexe != 'masculin') {
                echo "checked";
            } ?>>Féminin
            </td>
        <tr>
            <th>Occupation</th>
            <td class="impair">
                <input type="checkbox" name="occup[]" value="enseignant" 
                <?php  if ($update) foreach ($occupation as $value) {
                   if (utf8_encode($value) == 'enseignant') {
                    echo "checked";
                    break;
                }
                } ?>>Enseignant</input>
                <input type="checkbox" name="occup[]" value="étudiant" <?php if ($update) foreach ($occupation as $value) {
                   if (utf8_encode($value) == 'étudiant') {
                    echo "checked";
                    break;
                }
                } ?>>Etudiant</input>
                <input type="checkbox" name="occup[]" value="ingénieur" <?php  if ($update) foreach ($occupation as $value) {
                   if (utf8_encode($value) == 'ingénieur') {
                    echo "checked";
                    break;
                }
                } ?>>Ingénieur</input>
                <input type="checkbox" name="occup[]" value="retraité" <?php  if ($update) foreach ($occupation as $value) {
                   if (utf8_encode($value) == 'retraité') {
                    echo "checked";
                    break;
                }
                } ?>>Retraité</input>
                <input type="checkbox" name="occup[]" value="chômeur" <?php  if ($update) foreach ($occupation as $value) {
                   if (utf8_encode($value) == 'chômeur') {
                    echo "checked";
                    break;
                }
                } ?>>Chômeur</input>
                <input type="checkbox" name="occup[]" value="autre" <?php  if ($update) foreach ($occupation as $value) {
                   if (utf8_encode($value) == 'autre') {
                    echo "checked";
                    break;
                }
                } ?>>Autre</input>
            </td>
        </tr>
        <tr>
            <th>URL</th>
            <td class="pair"><input class="fill" type="url" name="url" id="" <?php if ($update) {
                echo "value='$url'";
            } ?>></td>
        </tr>

        <tr>
            <th class="impair"></td>
            <td class="impair"><input type="submit" value="Envoyer"><input type=button onclick="location.href='liste.php'" value="Annuler"></td>
        </tr>
    </tbody>
    </table>
</form>

</body>

</html>
